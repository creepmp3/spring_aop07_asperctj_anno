<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="<c:url value='/css/table.css'/>" />
</head>
<body>
    <h1>boardList</h1>
    
    <div class="list">
	    <table>
	        <thead>
		        <tr>
		            <th>번호</th>
		            <th>제목</th>
		            <th>작성자</th>
		            <th>조회수</th>
		            <th>추천수</th>
		            <th>등록일</th>
		        </tr>
		    </thead>
		    <tbody>
		        <c:forEach items="${list }" var="dto">
		        <tr>
		            <td><c:out value="${dto.bno }"/></td>
		            <td><a href="<c:url value='/board/view?bno=${dto.bno }'></c:url>"><c:out value="${dto.title }"/></a></td>
		            <td><a href="<c:url value='/board/view?bno=${dto.bno }'></c:url>"><c:out value="${dto.writer }"/></a></td>
		            <td><c:out value="${dto.readcount }"/></td>
		            <td><c:out value="${dto.hits }"/></td>
		            <td><c:out value="${dto.regdate }"/></td>
		        </tr>
		        </c:forEach>
	        </tbody>
	    </table>
	    <div class="btnArea">
	        <a href="<c:url value='/board/write'></c:url>" class="btn">글쓰기</a>
	    </div>
	</div>
</body>
</html>